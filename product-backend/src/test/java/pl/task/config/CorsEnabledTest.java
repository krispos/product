package pl.task.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import pl.task.product.ProductDto;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"pl.task.cors.allow=true"})
public class CorsEnabledTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void testCorsEnabled() {
        var response = testRestTemplate.getForEntity("/products", ProductDto[].class);
        assertThat(response).isNotNull();
        var headers = response.getHeaders();
        var varyHeader = headers.get("Vary");
        assertThat(varyHeader).isNotNull();
        assertThat(varyHeader).contains("Origin");
    }

}
