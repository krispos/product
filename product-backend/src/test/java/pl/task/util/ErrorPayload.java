package pl.task.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorPayload {

    private int status;
    private String message;
    private String error;
    private String path;
    private String timestamp;
}
