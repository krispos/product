package pl.task.product;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import pl.task.util.ErrorPayload;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @BeforeEach
    public void setUp() {
        productRepository.deleteAll();

        var product1 = new Product("Product 1", new BigDecimal("100.00"));
        var product2 = new Product("Product 2", new BigDecimal("0.01"));
        var product3 = new Product("Product 3", new BigDecimal("333.33"));

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
    }

    @AfterEach
    public void tearDown() {
        productRepository.deleteAll();
    }

    @Test
    void testGetAllProducts() {
        var products = testRestTemplate.getForObject("/products", ProductDto[].class);

        assertThat(products).isNotNull();
        assertThat(products).hasSize(3);
    }

    @Test
    void testGetProductById() {
        var id = getProductId("Product 2");
        var product = testRestTemplate.getForObject("/products/" + id, ProductDto.class);

        assertThat(product).isNotNull();
        assertThat(product.getId()).isEqualTo(id);
        assertThat(product.getName()).isEqualTo("Product 2");
        assertThat(product.getPrice()).isEqualTo(new BigDecimal("0.01"));
    }

    @Test
    void testGetNonExistentProductById() {
        var response = testRestTemplate.getForEntity("/products/" + getFreeProductId(), String.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).contains("Product not found");
    }

    @Test
    void testGetProductByIncorrectId() {
        var response = testRestTemplate.getForEntity("/products/zzz", ErrorPayload.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        var error = response.getBody();
        assertThat(error).isNotNull();
        assertThat(error.getMessage()).contains("Failed to convert value");
    }


    @Test
    void testDeleteProduct() {
        var products = testRestTemplate.getForObject("/products", ProductDto[].class);
        assertThat(products).hasSize(3);

        var id = getProductId("Product 3");
        testRestTemplate.delete("/products/" + id);

        products = testRestTemplate.getForObject("/products", ProductDto[].class);
        assertThat(products).hasSize(2);

        var product = testRestTemplate.getForEntity("/products/" + id, ErrorPayload.class);
        assertThat(product.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void testAddProduct() {
        var product = new ProductDto("Product 4", new BigDecimal("444.44"));
        var savedProduct = testRestTemplate.postForObject("/products", product, ProductDto.class);

        assertThat(savedProduct).isNotNull();
        assertThat(savedProduct.getId()).isNotNull();
        assertThat(savedProduct.getName()).isEqualTo("Product 4");
        assertThat(savedProduct.getPrice()).isEqualTo(new BigDecimal("444.44"));
    }

    @Test
    void testAddInvalidProduct() {
        var personJson = """
                {
                    "name": "Product 5",
                    "price": "invalid"
                }
                """;
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var request = new HttpEntity<>(personJson, headers);

        var response = testRestTemplate.postForEntity("/products", request, ErrorPayload.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        var error = response.getBody();
        assertThat(error).isNotNull();
        assertThat(error.getMessage()).contains("JSON parse error: Cannot deserialize value");
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  "})
    void testAddInvalidProductName(String name) {
        var personJson = """
                {
                    "name": %s,
                    "price": "100"
                }
                """.formatted(name == null ? null : "\"" + name + "\"");
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        var request = new HttpEntity<>(personJson, headers);

        var response = testRestTemplate.postForEntity("/products", request, ErrorPayload.class);
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        var error = response.getBody();
        assertThat(error).isNotNull();
        assertThat(error.getMessage()).contains("Validation failed for object='productDto'");
    }


    private Long getProductId(String name) {
        var products = testRestTemplate.getForObject("/products", ProductDto[].class);
        return Arrays.stream(products)
                .filter(product -> name.equals(product.getName()))
                .map(ProductDto::getId)
                .findFirst().orElseThrow(() -> new RuntimeException("Product not found"));
    }

    private Long getFreeProductId() {
        var products = testRestTemplate.getForObject("/products", ProductDto[].class);
        return Arrays.stream(products)
                .map(ProductDto::getId)
                .max(Long::compareTo).orElse(0L) + 1;
    }
}
