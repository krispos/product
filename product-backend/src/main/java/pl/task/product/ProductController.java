package pl.task.product;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@AllArgsConstructor
@RestController
public class ProductController {

    private ProductService productService;

    @GetMapping("/products")
    public Iterable<ProductDto> getAllProducts() {
        return StreamSupport.stream(productService.getAllProducts().spliterator(), false)
                .map(ProductDto::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/products/{id}")
    public ProductDto getProductById(@PathVariable Long id) {
        try {
            return ProductDto.toDto(productService.getProductById(id));
        } catch (ProductService.ProductNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

    @PostMapping("/products")
    public ProductDto addProduct(@Valid @RequestBody ProductDto product) {
        Product saved = productService.addProduct(Product.toProduct(product));
        return ProductDto.toDto(saved);
    }
}
