import React, {ReactNode} from "react";
import {QueryClient, QueryClientProvider} from "@tanstack/react-query";

export const wrapWithProvider = (children: ReactNode) => {

    const queryClient = new QueryClient({
        defaultOptions: {
            queries: {
                retry: false,
            },
        },
    })
    return <QueryClientProvider client={queryClient}>
        {children}
    </QueryClientProvider>

}