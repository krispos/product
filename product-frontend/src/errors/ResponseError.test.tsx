import React from "react";
import {ErrorResponseMessage} from "./ResponseError";
import {render, screen} from "@testing-library/react";


test("Present error message", () => {
    expect(1).toBe(1)

    render(<ErrorResponseMessage status={400} statusText="Bad Request" data="Invalid request"/>)
    expect(screen.getByText(/400/i)).toBeInTheDocument();
    expect(screen.getByText(/Bad Request/i)).toBeInTheDocument();
    expect(screen.getByText(/Invalid request/i)).toBeInTheDocument();
});

test("Present error message with api error response", () => {
    const apiErrorResponse = {
        timestamp: "2021-09-14T12:00:00",
        status: 400,
        error: "Bad Request Api",
        path: "/products",
        message: "Invalid request"
    }

    render(<ErrorResponseMessage status={400} statusText="Bad Request" data={apiErrorResponse}/>)
    expect(screen.getByText(/400/i)).toBeInTheDocument();
    expect(screen.getByText(/Bad Request/i)).toBeInTheDocument();
    expect(screen.getByText(/Invalid request/i)).toBeInTheDocument();
    expect(screen.queryByText(/2021-09-14T12:00:00/i)).not.toBeInTheDocument();
    expect(screen.queryByText(/\/products/i)).not.toBeInTheDocument();
});