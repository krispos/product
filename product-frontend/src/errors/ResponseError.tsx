import React from "react";
import "./ResponseError.css";

export interface ErrorResponse {
    status: number;
    statusText: string;
    data: string | ApiErrorResponse;

}

export interface ApiErrorResponse {
    timestamp: string;
    status: number;
    error: string;
    path: string;
    message: string;
}

export const ErrorResponseMessage = (props: ErrorResponse) => {
    const message = typeof props.data === 'string' ? props.data : props.data.message;

    return <div className="response-error">
        <p>{props.status} {props.statusText} <br/> {message}</p>
    </div>
}



