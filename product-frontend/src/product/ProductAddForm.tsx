import Popup from "reactjs-popup";
import axios from "axios";
import React, {useState} from "react";
import "./ProductAddForm.css";
import {ErrorResponseMessage, ErrorResponse} from "../errors/ResponseError";

interface ProductAddFormProps {
    added: () => void;
}

const ProductAddForm = (props: ProductAddFormProps) => {

    const [name, setName] = useState<string>('');
    const [price, setPrice] = useState<string>('');
    const [open, setOpen] = useState<boolean>(false);

    const [errorResponse, setErrorResponse] = useState<ErrorResponse | null>(null)

    const addProduct = () => {
        axios.post('/products', {
            name: name,
            price: price
        }).then(res => {
            setOpen(false);
            props.added();
        }).catch(err => {
            setErrorResponse(err.response);
        })
    }

    const handleNameChange = (e: React.FormEvent<HTMLInputElement>) => {
        setName(e.currentTarget.value);
    }

    const handlePriceChange = (e: React.FormEvent<HTMLInputElement>) => {
        setPrice(e.currentTarget.value);
    }

    const onClose = () => {
        setOpen(false);
        setErrorResponse(null);
        setPrice('');
        setName('');
    }

    const onOpen = () => {
        setOpen(true)
    }

    const canAdd = () => {
        return isFilled(name) && isFilled(price) && isPriceValid(price)
    }

    const isFilled = (value: string) => {
        return value !== '';
    }

    const isPriceValid = (value: string) => {
        const regex = /^\d+(\.\d{1,2})?$/;
        return regex.test(value);
    }

    return (
        <div className="product-add-form">
                <Popup trigger={<button className="product-add-form-open">Add +</button>} modal={true}
                       onClose={() => onClose()} open={open} onOpen={onOpen}>
                    <div>
                        <h3>Add Product</h3>
                        <div className="add-form-inputs">
                            <label htmlFor="nameInput">Name</label>
                            <input id="nameInput" type="text" value={name} onChange={handleNameChange}/>

                            <label htmlFor="priceInput">Price</label>
                            <input id="priceInput" className={isPriceValid(price) ? "" : "price-invalid"}
                                   type="text" value={price}
                                   onChange={handlePriceChange}/>
                        </div>
                        <button className="add-button" onClick={() => addProduct()} disabled={!canAdd()}>Add</button>
                        {errorResponse && <ErrorResponseMessage {...errorResponse} />}
                    </div>
                </Popup>
        </div>
    );
}


export default ProductAddForm;