import ProductPage from "./ProductPage";
import React from "react";
import {fireEvent, render, screen, waitFor, waitForElementToBeRemoved} from "@testing-library/react";

import axios from "axios";
import {wrapWithProvider} from "../test_utils/test_util";
import {act} from "react-dom/test-utils";

jest.mock("axios");

test("Load empty product page", async () => {

    (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValue({data: []});

    render(wrapWithProvider(<ProductPage/>));

    await waitFor(() => screen.findByText(/Product page/i));
    expect(screen.queryByText(/Remove/i)).not.toBeInTheDocument();

});

test("Load product page with products", async () => {
    (axios.get as jest.MockedFunction<typeof axios.get>).mockResolvedValue({
        data: [
            {id: 1, name: "Product 1", price: 100},
            {id: 2, name: "Product 2", price: 200},
            {id: 3, name: "Product 3", price: 300}]
    });
    render(wrapWithProvider(<ProductPage/>));
    await waitFor(() => screen.findByText(/Product page/i));
    expect(screen.getByText(/Product 1/i)).toBeInTheDocument();
    expect(screen.getByText(/100/i)).toBeInTheDocument();
    expect(screen.getByText(/Product 2/i)).toBeInTheDocument();
    expect(screen.getByText(/200/i)).toBeInTheDocument();
    expect(screen.getByText(/Product 3/i)).toBeInTheDocument();
    expect(screen.getByText(/300/i)).toBeInTheDocument();

    expect(screen.getAllByText(/Remove/i).length).toBe(3);

    expect(screen.getAllByText(/Remove/i).length).toBe(3);

});

test("Remove product", async () => {
    // requests scenario is:
    // 1. Load 3 products
    // 2. Remove product 2
    // 3. Load 2 products
    const responses: Promise<any | undefined>[] = [
        Promise.resolve({
            data: [
                {id: 1, name: "Product 1", price: 100},
                {id: 2, name: "Product 2", price: 200},
                {id: 3, name: "Product 3", price: 300}]
        }),
        Promise.resolve({
            data: [
                {id: 1, name: "Product 1", price: 100},
                {id: 3, name: "Product 3", price: 300}]
        })
    ];

    (axios.get as jest.MockedFunction<typeof axios.get>).mockImplementation((url: string): Promise<any> => {
        return responses.shift() || Promise.resolve({data: []});
    });

    let removeRequested = false;
    (axios.delete as jest.MockedFunction<typeof axios.delete>).mockImplementation((url: string): Promise<any> => {
        expect(url).toBe("/products/2");
        removeRequested = true;
        return Promise.resolve({});
    });

    render(wrapWithProvider(<ProductPage/>));
    await waitFor(() => screen.findByText(/Product page/i));
    expect(screen.getByText(/Product 2/i)).toBeInTheDocument();

    const removeButtons = await screen.findAllByText(/Remove/i);
    expect(removeButtons.length).toBe(3);
    removeButtons[1].click();
    expect(removeRequested).toBeTruthy();

    await waitForElementToBeRemoved(() => screen.queryByText(/Product 2/i));
    expect(responses.length).toBe(0);

});

test("Add product", async () => {
    // requests scenario is:
    // 1. Load 3 products
    // 2. Add product 4
    // 3. Load 4 products
    const responses: Promise<any | undefined>[] = [
        Promise.resolve({
            data: [
                {id: 1, name: "Product 1", price: 100},
                {id: 2, name: "Product 2", price: 200},
                {id: 3, name: "Product 3", price: 300}]
        }),
        Promise.resolve({
            data: [
                {id: 1, name: "Product 1", price: 100},
                {id: 2, name: "Product 2", price: 200},
                {id: 3, name: "Product 3", price: 300},
                {id: 4, name: "Product 4", price: 400}]
        })
    ];

    (axios.get as jest.MockedFunction<typeof axios.get>).mockImplementation((url: string): Promise<any> => {
        return responses.shift() || Promise.resolve({data: []});
    });

    let addRequested = false;
    (axios.post as jest.MockedFunction<typeof axios.post>).mockImplementation((url: string, data: any): Promise<any> => {
        expect(url).toBe("/products");
        expect(data).toEqual({name: "Product 4", price: "400"});
        addRequested = true;
        return Promise.resolve({id: 4, name: "Product 4", price: 400});
    });

    render(wrapWithProvider(<ProductPage/>));

    await waitFor(() => screen.findByText(/Product page/i));
    expect(screen.getByText(/Product 3/i)).toBeInTheDocument();

    // open popup, fill values and submit
    const addButton = screen.getByText(/Add +/i);
    act(() => {
            addButton.click();
        }
    );

    await waitFor(() => screen.findByText(/Add Product/i));


    const nameInput = screen.getByLabelText(/Name/i);
    fireEvent.change(nameInput, {target: {value: "Product 4"}});
    expect(nameInput).toHaveValue("Product 4");

    const priceInput = screen.getByLabelText(/Price/i);
    fireEvent.change(priceInput, {target: {value: "400"}});
    expect(priceInput).toHaveValue("400");

    const button = await screen.findAllByText(/Add/i)
        .then(e => e.filter(e => e.textContent === "Add"))
        .then(e => e[0])

    act(() => {
        button.click();
    });

    // verify that product is added
    await waitFor(() => expect(screen.getAllByText(/Remove/i).length).toBe(4));

    // popup seems not to go away
    // await waitForElementToBeRemoved(() => screen.queryByText(/Add product/i), {timeout: 3000});

    expect(addRequested).toBeTruthy();
    expect(responses.length).toBe(0);
    expect(screen.getByText(/Product 4/i)).toBeInTheDocument();


});