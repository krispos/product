import React, {useState} from "react";
import {useQuery} from "@tanstack/react-query";
import axios from "axios";
import Product from "./Product";
import ProductAddForm from "./ProductAddForm";
import "./ProductPage.css"
import {ErrorResponseMessage, ErrorResponse} from "../errors/ResponseError";


const ProductPage = () => {
    const [errorResponse, setErrorResponse] = useState<ErrorResponse | null>(null)

    const getProducts = useQuery({
        queryKey: ["products"],
        queryFn: () =>
            axios.get('/products')
                .then(res => {
                    return res;
                })
                .then(res => res.data)
                .catch(err => {
                    setErrorResponse(err.response);
                    return [];
                })
    })

    const removeProduct = (id: number) => {
        axios.delete(`/products/${id}`)
            .then(res => {
                getProducts.refetch();
            })
            .catch(err => {
                setErrorResponse(err.response);
            })
    }

    if (getProducts.isLoading) return <div>Loading...</div>
    if (getProducts.isError) return <div>Error {getProducts.error.message} {getProducts.error.stack}</div>

    return <div className="product-page">
        <h2>Product page</h2>
        <ProductAddForm added={() => getProducts.refetch()}/>

        <div className="product-list">
            <div className="product-list-header">
                <div>Name</div>
                <div>Price</div>
            </div>

            {getProducts.data.map((product: Product) => (
                <div className="product-list-item" key={product.id}>
                    <div>{product.name}</div>
                    <div>{product.price}</div>
                    <button className="product-list-item-remove" onClick={() => removeProduct(product.id)}>Remove</button>
                </div>
            ))}
        </div>
        {errorResponse && <ErrorResponseMessage {...errorResponse} />}
    </div>;
}


export default ProductPage;
